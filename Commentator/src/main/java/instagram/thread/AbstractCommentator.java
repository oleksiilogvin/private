package instagram.thread;

import instagram.command.CommentCommand;
import instagram.command.LogInCommand;
import instagram.constant.Settings;
import instagram.exception.InvalidLastCommentException;
import instagram.scanner.CommentProcessor;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;
import java.text.DecimalFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.Temporal;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public abstract class AbstractCommentator implements Callable<String>
{

    protected int min;
    protected int max;
    protected static AtomicInteger counter = new AtomicInteger();
    protected String login;
    protected String password;
    protected static Temporal startTime;

    public AbstractCommentator(int min, int max, String login, String password)
    {
        this.min = min;
        this.max = max;
        this.login = login;
        this.password = password;
    }

    @Override
    public String call()
    {
        try
        {
            Thread.currentThread().setName(login);
            WebDriver driver = new RemoteWebDriver(new URL(Settings.CHROME_DRIVER), new ChromeOptions());
//            WebDriver driver = new FirefoxDriver();
            driver.get(Settings.LOGIN_PAGE);
            Actions actions = new Actions(driver);
            new LogInCommand(login, password).execute(driver, actions);
            CommentProcessor commentProcessor = new CommentProcessor(driver);
            while (true)
            {
                processPromosBatch(driver, actions, commentProcessor);
            }
        } catch (Exception e)
        {
            log.error("Thread: {}, Error message: {}", Thread.currentThread().getName(), e);
        }
        return "Exit";
    }

    private void processPromosBatch(WebDriver driver, Actions actions, CommentProcessor commentProcessor) throws InterruptedException
    {
        Set<String> promos = Settings.promos.keySet();
        for (String promo : promos)
        {
            driver.get(promo);
            processComments(driver, commentProcessor, actions);
            makeRandomPause(actions, min, max);
        }
    }

    protected abstract void processComments(WebDriver driver, CommentProcessor commentProcessor, Actions actions) throws InterruptedException;

    protected void postComment(String comment, WebDriver driver, Actions actions) throws InterruptedException
    {
        new CommentCommand(comment).execute(driver, actions);
    }

    protected void makeRandomPause(Actions actions, int from, int to)
    {
        actions.pause(Duration.ofMillis(ThreadLocalRandom.current().nextInt(from, to + 1))).perform();
    }

    protected void logResult(WebDriver driver, boolean isBlocked, String lastComment, boolean isSent)
    {
        String promo = Settings.promos.get(driver.getCurrentUrl());
        if (isSent && !isBlocked)
        {
            double hoursPassedFromStart = Duration.between(startTime, Instant.now()).toMinutes() / 60D;
            double totalComments = counter.incrementAndGet();
            DecimalFormat format = new DecimalFormat("#.##");
            log.info("Account: {}, Promo: {}, Their: {}, My: {}, Total comments: {}, Time passed: {} hours, Comments/h: {}",
                    login, promo, Integer.parseInt(lastComment) - 1, lastComment, (long) totalComments, format.format(hoursPassedFromStart),
                    format.format(totalComments / hoursPassedFromStart));
        }
    }

    public static void setStartTime(Temporal startTime)
    {
        AbstractCommentator.startTime = startTime;
    }
}
