package instagram.command;

import instagram.constant.CSSselectors;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import java.time.Duration;

import static instagram.constant.CSSselectors.PASSWORD_SELECTOR;
import static instagram.constant.CSSselectors.USERNAME_SELECTOR;

public class LogInCommand implements Command {

    private String login;
    private String password;

    public LogInCommand(String login, String password) {
        this.login = login;
        this.password = password;
    }

    @Override
    synchronized public void execute(WebDriver driver, Actions actions) {
        actions.pause(Duration.ofSeconds(2)).perform();
        actions.moveToElement(driver.findElement(USERNAME_SELECTOR)).click().sendKeys(login);
        actions.pause(Duration.ofSeconds(2));
        actions.moveToElement(driver.findElement(PASSWORD_SELECTOR)).click().sendKeys(password).perform();
        actions.pause(Duration.ofSeconds(2)).perform();
        driver.findElement(CSSselectors.LOGIN_FORM).submit();
        actions.pause(Duration.ofSeconds(2)).perform();
    }
}
