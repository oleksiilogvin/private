package instagram.constant;

import org.openqa.selenium.By;

public class CSSselectors {
    public static final By LOGIN_BUTTON = By.cssSelector("button[class='_5f5mN']");
    public static final By BLOCKED_POPUP = By.cssSelector("p[class='_fb78b']");
    public static final By LOGIN_FORM = By.cssSelector("form[class='HmktE']");
    public static final By COMMENT_SUBMIT = By.cssSelector("form[class='X7cDz']");
    public static final By COMMENTS_FIELD = By.cssSelector("textarea[placeholder='Add a comment…']");
    public static final By COMMENTS_FIELD_2 = By.cssSelector("textarea[class='Ypffh']");
//    public static final By LAST_COMMENT_SELECTOR = By.cssSelector("li[class='gElp9']:last-child>span");
    public static final By LAST_COMMENT_SELECTOR = By.cssSelector("li.gElp9:last-child div.P9YgZ div.C7I1f.X7jCj div.C4VMK span:nth-child(2) > a.notranslate");
    public static final By USERNAME_SELECTOR = By.name("username");
    public static final By PASSWORD_SELECTOR = By.name("password");
    public static final By SHOW_MORE_COMMENTS_SELECTOR = By.className("vTJ4h");
}
